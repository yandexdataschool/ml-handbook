let tooltips = document.querySelectorAll('.tooltip');
tooltips.forEach(item => {
	let tooltip_cds = item.getBoundingClientRect();
	let tooltip_text = item.children[0].children[0];
	let tooltip_text_cds = tooltip_text.getBoundingClientRect();
	if (document.body.scrollHeight - tooltip_cds.y <= tooltip_text_cds.height) {
	  tooltip_text.style.bottom = tooltip_text_cds.height + 10 + 'px';
	} else {
	  tooltip_text.style.top = tooltip_cds.height + 10 + 'px';
	}
	tooltip_text.style.left = tooltip_cds.left + 5 + 'px';
})