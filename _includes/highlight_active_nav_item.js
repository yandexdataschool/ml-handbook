window.addEventListener('DOMContentLoaded', () => {
    let elements = {};
    let last_active_id = null;
    document.querySelectorAll('h1[id]').forEach((section) => {
        elements[section.getAttribute('id')] = ['h1', section];
    });
    document.querySelectorAll('h2[id]').forEach((section) => {
        elements[section.getAttribute('id')] = ['h2', section];
    });
    document.addEventListener('scroll', () => {
        let max_negative = null;
        let candidate = null;
        for (const id in elements) {
            let class_end_elem = elements[id];
            let pos = class_end_elem[1].getBoundingClientRect().top;
            if (pos < 100 && (max_negative == null || pos > max_negative)) {
                max_negative = pos;
                candidate = id;
            }
        }
        let li;
        if (candidate != null && candidate !== last_active_id) {
            // console.log('INSIDE')
            if (last_active_id != null) {
                li = document.querySelector(`ul li a[href="#${last_active_id}"]`).parentElement;
                li.classList.remove('active');
                console.log('rem', li.children.length);
                if (elements[last_active_id][0] === 'h1') {
                    if (li.children.length > 1) {
                        li.children.item(1).classList.add('closed');
                    }
                } else {
                    li.parentElement.classList.add('closed');
                }
            }
            last_active_id = candidate;

            li = document.querySelector(`ul li a[href="#${last_active_id}"]`).parentElement;
            li.classList.add('active');
            console.log('add', li.children.length);
            if (elements[last_active_id][0] === 'h1') {
                if (li.children.length > 1) {
                    li.children.item(1).classList.remove('closed');
                }
            } else {
                li.parentElement.classList.remove('closed');
            }
        } else if (candidate == null && last_active_id != null) {
            // console.log('OUTSIDE')
            document.querySelector(`ul li a[href="#${last_active_id}"]`).parentElement.classList.remove('active');
            last_active_id = null;
        }
    })
});