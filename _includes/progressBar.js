let progress = document.getElementById("progress");
let content = document.querySelector(".container.content");

handleScroll = () => {
    // начало контента смещается на длину предваряющего блока
    let scrollValue = Math.max(window.scrollY - 106, 0);
    let contentHeight = content.clientHeight - window.innerHeight;
    progress.style.width = (scrollValue / contentHeight) * 100 + "%";
}

window.addEventListener("scroll", handleScroll);
