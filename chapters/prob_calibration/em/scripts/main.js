'use strict';

const canvas = document.querySelector('.graph canvas');
const ctx = canvas.getContext('2d');
const xMin = -100, xMax = 100;
const yMin = -100, yMax = 100;

const clusterColors = [
    '#FFC100',
    '#348FEA',
    '#97C804',
    '#4CB9C0'
];

const points = [];

let gmm = null;

let point;

function generateGroup() {

    // TODO: random radius..
    let a = 10 + Math.random() * 40;
    let b = 10 + Math.random() * 40;

    let centerX = 100 - Math.random() * 200;
    let centerY = 100 - Math.random() * 200;

    for (let i = 0; i < 100; ++i) {

        let r = a * Math.random();
        let fi = 2 * Math.PI * Math.random();
        let x = centerX + r * Math.cos(fi);
        let y = centerY + b / a * r * Math.sin(fi);

        points.push([x, y]);
    }

}

function generatePoints() {
    for (let i = 0; i < 2 + Math.random() * 7; ++i) {
        generateGroup();
    }
}

generatePoints();

canvas.addEventListener('click', function (e) {
    let w = canvas.width;
    let h = canvas.height;

    let p = [e.offsetX / w * (xMax - xMin) + xMin, e.offsetY / h * (yMax - yMin) + yMin];

    point = points.filter(point => p[0] - 2 <= point[0] && point[0] <= p[0] + 2 && p[1] - 2 <= point[1] && point[1] <= p[1] + 2)[0];
    probability();

    redraw();
});


function probability() {
    if(!point&&gmm) {
        for (let i = 0; i < gmm.clusters; ++i) {
            let div = document.getElementById("cluster-" + i);
            let r = div.querySelector(".resp-"+i);
            r.style.display = "none";
        }
    }

    if (point&&gmm) {
        for (let i = 0; i < gmm.clusters; ++i) {
            let div = document.getElementById("cluster-" + i);
            let r = div.querySelector(".resp-"+i);
            r.style.display = "";
            r.textContent = "probability "+gmm.cResps[i][points.indexOf(point)].toFixed(2);
        }
    }
}


const yAxis = document.querySelector('.graph .y-axis');
const canvasWrap = document.querySelector('.graph .canvas-wrapper');
const canvasWrapStyle = window.getComputedStyle(canvasWrap, null);

const draw = new Draw(canvas, xMin, xMax, yMin, yMax);

resizeGraph();
window.addEventListener('resize', resizeGraph);

function resizeGraph() {
    let w = parseFloat(canvasWrapStyle.getPropertyValue('width'));
    let paddingX = parseFloat(canvasWrapStyle.getPropertyValue('padding-left'));
    let paddingY = parseFloat(canvasWrapStyle.getPropertyValue('padding-top'));

    let h = w - 2 * paddingX + 2 * paddingY;
    canvasWrap.style.height = h + 'px';
    yAxis.style.height = h + 'px';

    canvas.width = canvas.scrollWidth;
    canvas.height = canvas.scrollHeight;

    redraw();
}

function redraw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (gmm) {
        let pointColors = points
            .map(p => gmm.predict(p))
            .map(probs => probs.reduce(
                (iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0
            ))
            .map(i => clusterColors[i]);

        pointColors[points.indexOf(point)] = 'black';

        for (let i = 0; i < gmm.clusters; i++) {
            draw.ellipse(gmm.means[i], gmm.covariances[i], clusterColors[i], i);
        }
        draw.points(points, pointColors);

        for (let i = 0; i < gmm.clusters; ++i) {
            let div = document.getElementById("cluster-" + i);
            let w = div.querySelector(".weight-" + i);
            let m = div.querySelector(".mean-" + i);
            let c = div.querySelector(".covariance-" + i);

            w.textContent = "weight: " + gmm.weights[i].toFixed(2);
            m.textContent = "mean: " + "(" + gmm.means[i].map(item => item.toFixed(2)).join(",  ") + ")";
            c.textContent = "covariance: " + gmm.covariances[i].map(item => "(" + item.map(item => item.toFixed(2)).join(", ") + ")").join(",  ");
        }

        // if (gmm.singularity) draw.singularity(gmm.singularity);
    } else {
        draw.points(points);
    }
}