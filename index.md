---
title: Содержание
layout: title
---

<div style="text-align: center; "> 
  <h1> Учебник по машинному обучению </h1> 
</div>
<div class="logo"> 
  <img src="./shad.svg" style="width: 100%"> 
</div>


Онлайн-учебник по машинному обучению от ШАД — для тех, кто не боится математики и хочет разобраться в технологиях ML. Вы изучите классическую теорию и тонкости реализации алгоритмов, пройдя путь от основ машинного обучения до тем, которые поднимаются в свежих научных статьях.

<div style="padding-bottom: 50px; padding-top: 20px;">
  <h2 style="padding-inline-start:40px;"> [Введение](chapters/intro/intro) </h2>

  <ol> 
  {% assign first_level_pages = site.data.pages %}
  {% for first_level_page in first_level_pages %}
    <li>
      {% if first_level_page.url != nil %}
      <h2> <a href="{{ first_level_page.url | absolute_url }}"> {{ first_level_page.title }} </a> </h2>
      {% else %}
      <h2> {{ first_level_page.title }} </h2>
      {% endif %}
      
      <ol> 
      {% assign sec_level_pages = first_level_page.subfolderitems %}
      {% for sec_level_page in sec_level_pages %}    
        <li> <a href="{{ sec_level_page.url | absolute_url }}">{{ sec_level_page.title }}</a> </li>
      {% endfor %}
      </ol>
    </li>
  {% endfor %}
  </ol> 
</div>

<div>
  В учебник будут добавляться новые главы, поэтому следите за обновлениями, а ещё лучше <a href="https://forms.yandex.ru/surveys/10035916.245bdebafd6fa751cd7ea4b25c91f532b966e71c/"> подписывайтесь на новости</a>. Оставляйте свои отзывы и пожелания в <a href="https://forms.yandex.ru/u/619271c0a33289f6627ec17d/"> форме</a>.
</div>
